<!-- .slide: data-state="cover" id="cover-page" data-timing="20" data-menu-title="Cover slide" -->
<div class="title">
    <h1>SEO</h1>
    <h2>Was ist SEO eigentlich? & Grundlagen von SEO</h2>
</div>

<div class="row presenters">
    <div class="presenter presenter-1">
        <h3 class="name">Enno Gotthold</h3>
        <h3 class="job-title">Engineering Apprentice</h3>
        <h3 class="email"><a href="mailto:egotthold@suse.de">egotthold@suse.de</a></h3>
    </div>
</div>


<!-- .slide: data-state="normal" id="agenda" -->
## Was dürft ihr erwarten?

<h3>
Take-Home-Lessen: SEO = Keine Magie = Search Engine Optimization
</h3>

Präsentation: https://school_guy.gitlab.io/azubimeeting-seo

*   Was ist SEO?
*   Was ist SEO nicht?
*   Google, Bing, Yahoo, ...
*   Einfache Tipps für SEO
