<!-- .slide: data-state="normal" id="what-is-seo" data-timing="30" -->
## Was ist SEO?

<h3>
SEO = Search Engine Optimization; dt: Suchmaschinenoptimierung
</h3>

*   Handlungen die eine Verbesserung in der natürlichen Reihenfolge einer Suchmaschine bewirken
*   Maßnahmen schließen direkten Trafic und bezahlte Werbung aus (unter anderem)
*   Verschiedene (priorisierte) Ziele möglich:
    *   Bilder
    *   Videos
    *   Vertikale Suchmaschinen
    *   ...


<!-- .slide: data-state="normal" id="what-is-seo-2" data-timing="30" -->
## Optimierungsarten

* Onpage: Alles was direkt auf der eigenen Website veränderbar ist
* Offpage: Backlinks & Social Signals
* Suchbegriffe: Texte oder ganze Webpages werden auf bst. Keywords optimiert
* Reverse Engineering: Algorithmen der Suchmaschinen sind zumeist nicht Open-Source
* Sonderfälle: Google Scholar bspw. statt Links sind Erwähnungen in andern wissenschaftlichen Publikationen wichtig


<!-- .slide: data-state="normal" id="what-seo-is-not" data-timing="30" -->
## Was ist SEO nicht?

*   SEO ist NICHT SEM (Search Engine Marketing; bezahlte Werbung)
*   SEO ist kein Übernacht Erfolg
*   SEO ist nicht nur eine Optimierung auf Google
*   SEO ist nicht nur mehr eine Optimierung auf das Site Ranking
*   SEO ist nicht Statisch
*   SEO ist nicht Cheaten!


Note: https://www.brickmarketing.com/blog/seo-is-not.htm && https://infomedia.com/blog/10-things-seo-is-not/
