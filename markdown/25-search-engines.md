<!-- .slide: data-state="normal" id="search-engines" data-timing="30" -->
## Suchmaschinen

*   Google
*   Bing
*   yahoo! (unterliegend wird Bing verwendet)
*   Baidu
*   u.v.m.


Note: https://www.searchenginejournal.com/seo-bing-vs-google/223363/#close


<!-- .slide: data-state="normal" id="search-engines-2" data-timing="30" -->
## Google

*   Keine Flash Inhalte & bevorzugt Text-Inhalte
*   Social-Media Diskussionen/Seiten werden wie normale Seiten behandelt
*   Backlinks von Seiten mit hohem Ranking wirken sich positiv auf das eigene Ranking aus
*   Meta-Keyword-Tag ist relativ/vollständig unbedeutend
*   Seiten die zusätzlich zu gutem Inhalt auch gute Keyword haben, sind positiv


<!-- .slide: data-state="normal" id="search-engines-3" data-timing="30" -->
## Bing

*   Crawlen von Flash-Only-Sites & Höheres Verständnis von Multimedia Inhalten
*   Social-Media Diskussionen haben einen großen Einfluss auf das Ranking
*   Backlink-Qualität wird an Domain-Alter/TLD/... festgemacht
*   Meta-Keyword-Tag und Meta-Description-Tag haben eventuell eine größere Bedeutung
*   Exact Keyword-Matches (Überoptimierung wird erkannt und negativ gezählt)
