<!-- .slide: data-state="normal" id="easy-tips" data-timing="30" -->
## Einfache Tipps

*   Informiert euch über Neuigkeiten
*   Übertreibt es nicht
*   Habt ein Seitenkonzept
*   Verlasst euch auf Hilfe aus Frameworks


<!-- .slide: data-state="normal" id="easy-tips-2" data-timing="30" -->
## Praktische Tipps

*   Einzigartige Title-Tags auf eurer Website
*   Kurze und prägnante Titel
*   Verwendet kurze prägnate Descriptions
*   Lesbare logische URL's
*   Eine Seite = Eine URL
*   Breadcrumbs für eine bessere Website Erfahrung
*   HTML & XML Sitemap
*   404 mit ausreichend Hilfetext
*   Kurze Ankertexte (aber Ausagekräftig)
*   Bilder in eigenem Ordner speichern
*   ...
